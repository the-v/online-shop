import React from "react";
import styles from "./Menu.module.css";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import {faBars} from "@fortawesome/free-solid-svg-icons";

function Menu() {
    return (
        <div className={styles.menu}>
            <div className={styles.contentSpace}>
                <form action="">
                    <input type="text" value="Search products..."/>
                    <FontAwesomeIcon icon={faSearch} className={styles.iconSearch} />
                </form>

                <FontAwesomeIcon icon={faBars} className={styles.iconBars} />
            </div>
        </div>
    );
}

export default Menu;