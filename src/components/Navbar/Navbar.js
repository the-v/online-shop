import React from "react";
import styles from "./Navbar.module.css";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUserAlt} from "@fortawesome/free-solid-svg-icons";
import {faLock} from "@fortawesome/free-solid-svg-icons";


function Navbar() {
    return (
        <div className={styles.navbar}>
            <div className={styles.contentSpace}>
                <div className={styles.navContent}>
                    <span className={styles.usd}>USD</span>
                    <span>English</span>
                    <span><a href="#">Help</a></span>
                </div>

                <div className={styles.navContent}>
                    <span><FontAwesomeIcon icon={faUserAlt} /><a href="#" className={styles.navLink}>Login</a></span>
                    <span><FontAwesomeIcon icon={faLock} /><a href="#" className={styles.navLink}>Register</a></span>
                </div>
            </div>
        </div>
    );
}

export default Navbar;