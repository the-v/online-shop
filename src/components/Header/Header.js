import React from "react";
import styles from "./Header.module.css";
import logotype from "../../img/Layer 1@1X.png";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faShoppingBasket} from "@fortawesome/free-solid-svg-icons";

function Header() {
    return (
        <div className={styles.header}>
            <div className={styles.contentSpace}>
                <div className={styles.numberPhone}><a href="#">2300 - 3560 - 222</a></div>
                <div className={styles.logotype}><img src={logotype} alt=""/></div>

                <div className={styles.basket}>
                    <FontAwesomeIcon icon={faShoppingBasket} className={styles.iconBasket} />
                    <div className={styles.numberProducts}><span>0</span></div>
                </div>
            </div>
        </div>
    );
}

export default Header;